import React from 'react';
import {fireEvent, render, screen, waitFor} from '@testing-library/react';
import axios from 'axios';
import App from './App'

export const mockedAxios = axios as jest.Mocked<typeof axios>;

describe('App Component', () => {
  beforeEach(() => {
    mockedAxios.get.mockClear();
  });

  test('renders without crashing', () => {
    render(<App/>);
    // @ts-ignore
    expect(screen.getByText('Files')).toBeInTheDocument();
    // @ts-ignore
    expect(screen.getByText('Result')).toBeInTheDocument();
  });

  test('fetches files and results on mount', async () => {
    mockedAxios.get.mockResolvedValueOnce({data: []}).mockResolvedValueOnce({data: []});

    render(<App/>);

    await waitFor(() => expect(mockedAxios.get).toHaveBeenCalledTimes(2));
    await waitFor(() => expect(mockedAxios.get).toHaveBeenCalledWith(`${process.env.REACT_APP_API_HOST}/files`));
    await waitFor(() => expect(mockedAxios.get).toHaveBeenCalledWith(`${process.env.REACT_APP_API_HOST}/files/results`));
  });

  test('renders tabs and handles tab switching', () => {
    render(<App/>);

    const filesTab = screen.getByText('Files');
    const resultsTab = screen.getByText('Result');

    // Initially, "Files" tab should be active
    // @ts-ignore
    expect(filesTab).toHaveClass('Mui-selected');
    // @ts-ignore
    expect(resultsTab).not.toHaveClass('Mui-selected');

    // Click on "Result" tab
    fireEvent.click(resultsTab);

    // Now, "Result" tab should be active
    // @ts-ignore
    expect(resultsTab).toHaveClass('Mui-selected');
    // @ts-ignore
    expect(filesTab).not.toHaveClass('Mui-selected');
  });
});
