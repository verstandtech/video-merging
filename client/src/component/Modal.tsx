import React from 'react';
import {Box, IconButton, Modal} from '@mui/material';
import CloseIcon from '@mui/icons-material/Close';

interface IModalProps {
  open: boolean;
  handleClose: () => void;
  content: JSX.Element;
}

const ModalWindow: React.FC<IModalProps> = ({open, handleClose, content}) => {
  return (
    <Modal
      open={open}
      onClose={handleClose}
      aria-labelledby="modal-window"
    >
      <Box
        sx={{
          position: 'absolute',
          top: '50%',
          left: '50%',
          transform: 'translate(-50%, -50%)',
          bgcolor: 'background.paper',
          boxShadow: 24,
          borderRadius: 2,
          p: 4,
        }}
      >
        <IconButton
          aria-label="close"
          onClick={handleClose}
          sx={{
            position: 'absolute',
            right: 8,
            top: 8,
            color: (theme) => theme.palette.grey[500],
          }}
        >
          <CloseIcon/>
        </IconButton>
        {content}
      </Box>
    </Modal>
  );
};

export default ModalWindow;