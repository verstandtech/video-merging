import React from 'react';

export interface IResultFile {
  id: string;
  title: string;
  filename: string;
  thumbnail: string;
  mimetype: string;
  add_time: string;
  update_time: string;
}

export interface IServerFile extends IResultFile {
  size?: string;
}

export interface VideoControlsProps {
  selectedFiles: IServerFile[];
  onFilesChange: React.Dispatch<React.SetStateAction<IServerFile[]>>;
  refreshFiles: () => void;
  refreshResults: () => void;
}

export interface ResultControlsProps {
  selectedResults: IResultFile[];
  onFilesChange: React.Dispatch<React.SetStateAction<IResultFile[]>>;
  refreshResults: () => void;
}

export interface IError {
  data: {
    message: string,
    error: string,
    statusCode: number,
  };

}