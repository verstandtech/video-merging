import React, {useState} from "react";
import Login from './Login';
import Signin from './Signin';
import {AppBar, Box, Button, Dialog, Toolbar, Typography} from "@mui/material";

type MainMenuProps = {
  isLoggedIn: boolean;
  onLogout: () => void;
};

const MainMenu: React.FC<MainMenuProps> = ({isLoggedIn, onLogout}) => {
  const [showLoginModal, setShowLoginModal] = useState(false);
  const [showRegisterModal, setShowRegisterModal] = useState(false);

  const openLoginModal = () => setShowLoginModal(true);
  const closeLoginModal = () => setShowLoginModal(false);

  const openRegisterModal = () => setShowRegisterModal(true);
  const closeRegisterModal = () => setShowRegisterModal(false);

  const onLogin = (data: { email: string, password: string }) => {
    // TODO Implement login logic here
    console.log("=>(MainMenu.tsx:27) data", data);
    // When ready to implement, this would set isLoggedIn to true:
    // isLoggedIn = true;
    closeLoginModal();
  };

  const logout = () => {
    // TODO need to create
    onLogout();
    return true;
  }

  const onRegister = (data: { email: string, password: string }) => {
    // TODO Implement register logic here
    console.log("=>(MainMenu.tsx:38) data", data);
    closeRegisterModal();
  };

  return (
    <Box sx={{flexGrow: 1}}>
      <AppBar position="static">
        <Dialog open={showLoginModal} onClose={closeLoginModal}>
          <Login onLogin={onLogin} onClose={closeLoginModal}/>
        </Dialog>
        <Dialog open={showRegisterModal} onClose={closeRegisterModal}>
          <Signin onRegister={onRegister} onClose={closeRegisterModal}/>
        </Dialog>
        <Toolbar>
          <Typography variant="h6" component="div" sx={{flexGrow: 1}}>
            Video merging system
          </Typography>

          {isLoggedIn ? (
            <div>
              <Button color="inherit" onClick={logout}>Logout</Button>
            </div>
          ) : (
            <div>
              <Button color="inherit" onClick={openLoginModal}>Login</Button>
              <Button color="inherit" onClick={openRegisterModal}>Register</Button>
            </div>
          )}
        </Toolbar>
      </AppBar>
    </Box>
  );
};

export default MainMenu;