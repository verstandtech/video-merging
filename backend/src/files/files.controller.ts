import {
  Body,
  Controller,
  Delete,
  Get,
  HttpCode,
  HttpStatus,
  NotFoundException,
  Param,
  Post,
  Query,
  Res,
  UploadedFiles,
  UseInterceptors,
} from '@nestjs/common';
import { FilesService } from './files.service';
import { FilesInterceptor } from '@nestjs/platform-express';
import { Files, Prisma, Result } from '@prisma/client';
import { ApiBody, ApiConsumes } from '@nestjs/swagger';
import * as path from 'path';
import * as fs from 'fs';
import { Response } from 'express';

const uploadDir = './uploads';
const thumbnailDir = './uploads/thumbnails';
const resultDir = `${uploadDir}/results/`;
const resultThumbnailDir = `${resultDir}/thumbnails/`;

@Controller('files')
export class FilesController {
  constructor(private readonly filesService: FilesService) {}

  @Post('upload')
  @ApiConsumes('multipart/form-data')
  @ApiBody({
    description: 'Video files',
    schema: {
      type: 'object',
      description: 'Video files',
      properties: {
        files: {
          type: 'array',
          items: {
            type: 'string',
            format: 'binary',
          },
        },
      },
    },
  })
  @HttpCode(HttpStatus.OK)
  @UseInterceptors(FilesInterceptor('files'))
  async uploadMultipleFiles(
    @UploadedFiles() files,
  ): Promise<Prisma.BatchPayload> {
    return await this.filesService.uploadFiles(files);
  }

  @Get()
  @HttpCode(HttpStatus.OK)
  async getFiles(@Query('filter') filter?: string): Promise<Files[]> {
    const result = await this.filesService.getFiles(filter);
    if (!result.length) throw new NotFoundException('Files not found');
    return result;
  }

  @Post('create')
  @HttpCode(HttpStatus.CREATED)
  async createFile(@Body() filenames: any): Promise<Result> {
    return await this.filesService.createFile(filenames.titles);
  }

  @Delete('delete')
  @HttpCode(HttpStatus.OK)
  async deleteVideo(@Body() fileList: any): Promise<Prisma.BatchPayload> {
    return await this.filesService.deleteVideo(fileList);
  }

  @Get('results')
  @HttpCode(HttpStatus.OK)
  async resultList(@Query('filter') filter?: string): Promise<Result[]> {
    const result = await this.filesService.getResultFiles(filter);
    if (!result.length) throw new NotFoundException('Files not found');
    return result;
  }

  @Delete('results/delete')
  @HttpCode(HttpStatus.OK)
  async deleteResult(@Body() fileList: any): Promise<Prisma.BatchPayload> {
    return await this.filesService.deleteResult(fileList);
  }

  @Get('results/download/:filename')
  async downloadFile(
    @Param('filename') filename: string,
    @Res() res: Response,
  ): Promise<void> {
    const filepath = path.join('uploads/results', filename);
    if (fs.existsSync(filepath)) {
      return res.download(filepath);
    } else {
      throw new Error('File does not exist.');
    }
  }

  @Get('results/:filename')
  async getResultFile(
    @Param('filename') filename: string,
    @Query('thumbnail') thumbnail: string,
    @Res() res: Response,
  ): Promise<void> {
    const result: Result = await this.filesService.getResult(filename);

    if (!result) throw new NotFoundException('Result file not found');

    const filePath =
      thumbnail === 'true'
        ? path.join(resultThumbnailDir, result.thumbnail)
        : path.join(resultDir, result.filename);

    res.setHeader(
      'Content-Type',
      thumbnail === 'true' ? 'image/jpeg' : result.mimetype,
    );

    res.setHeader('Cross-Origin-Resource-Policy', 'cross-origin');

    fs.createReadStream(filePath).pipe(res);
  }

  @Get('/file/:filename')
  async getFile(
    @Param('filename') filename: string,
    @Query('thumbnail') thumbnail: string,
    @Res() res: Response,
  ): Promise<void> {
    const result: Files = await this.filesService.getFile(filename);

    if (!result) throw new NotFoundException('File not found');

    const filePath =
      thumbnail === 'true'
        ? path.join(thumbnailDir, result.thumbnail)
        : path.join(uploadDir, result.filename);

    res.setHeader(
      'Content-Type',
      thumbnail === 'true' ? 'image/jpeg' : result.mimetype,
    );

    res.setHeader('Cross-Origin-Resource-Policy', 'cross-origin');

    fs.createReadStream(filePath).pipe(res);
  }
}
