import React from 'react';
import VideoPreview from './VideoPreview';
import {Box} from '@mui/material';
import {IServerFile} from "./types";
import Empty from "./Empty";

type VideoListProps = {
  files: IServerFile[];
  selectedFiles: IServerFile[];
  onFileSelect: (file: IServerFile) => void;
};

class VideoList extends React.Component<VideoListProps> {
  render() {
    let {
      files,
      selectedFiles,
      onFileSelect,
    } = this.props;

    if (!Array.isArray(files)) return <Empty response={files}/>

    return (
      <Box sx={{
        display: 'flex',
        flexDirection: 'row',
        flexWrap: 'wrap',
        gap: '16px',
        overflowY: 'auto',
        width: '100%',
        height: '100vh',
        minHeight: '300px',
      }}>
        {files.map((file) =>
          <Box sx={{minWidth: '300px', height: '300px'}}>
            <VideoPreview
              file={file}
              key={file.filename}
              isSelected={selectedFiles.includes(file)}
              onSelect={onFileSelect}
            />
          </Box>
        )}
      </Box>
    );
  }
}

export default VideoList;