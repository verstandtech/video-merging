import { Module } from '@nestjs/common';
import { FilesController } from './files.controller';
import { FilesService } from './files.service';
import { PrismaService } from '../prisma/prisma.service';

@Module({
  providers: [FilesService, PrismaService],
  controllers: [FilesController],
})
export class FilesModule {}
