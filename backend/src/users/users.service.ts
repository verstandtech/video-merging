import {
  Injectable,
  NotFoundException,
  UnauthorizedException,
} from '@nestjs/common';
import { PrismaService } from '../prisma/prisma.service';
import { Prisma, Users } from '@prisma/client';

@Injectable()
export class UsersService {
  constructor(private prisma: PrismaService) {}

  async createUser(dto: Prisma.UsersCreateInput): Promise<Users> {
    return this.prisma.users.create({ data: dto });
  }

  async getUsers(filter: string): Promise<Users[]> {
    return this.prisma.users.findMany({
      where: {
        email: { contains: filter },
      },
    });
  }

  async getUser(dto: Prisma.UsersWhereUniqueInput): Promise<Users> {
    const user = await this.prisma.users.findUnique({
      where: dto,
    });
    if (!user) throw new NotFoundException('User not found');
    return user;
  }

  async login(email: string, password: string): Promise<Users> {
    const user = await this.prisma.users.findUnique({
      where: {
        email,
      },
    });

    if (!user) throw new NotFoundException('User not found');
    if (password !== user.password)
      throw new UnauthorizedException('Incorrect password');

    return user;
  }

  async updateUser(
    email: string,
    dto: Prisma.UsersUpdateInput,
  ): Promise<Users> {
    const user = await this.prisma.users.findFirst({ where: { email } });
    if (!user) throw new NotFoundException('User not found');

    return this.prisma.users.update({
      where: { email },
      data: dto,
    });
  }
}
