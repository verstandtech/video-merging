import React, {FC, useState} from 'react';
import {Box, Button, DialogActions, DialogContent, DialogTitle, TextField} from "@mui/material";

interface LoginProps {
  onLogin: (data: { email: string; password: string }) => void;
  onClose: () => void;
}

const Login: FC<LoginProps> = ({onLogin, onClose}) => {
  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");

  const handleLogin = (event: React.FormEvent) => {
    event.preventDefault();
    onLogin({email, password});
  };

  return (
    <Box component="form" onSubmit={handleLogin} autoComplete="off">
      <DialogTitle>
        Login form
      </DialogTitle>
      <DialogContent>
        <TextField
          autoFocus
          required
          margin="dense"
          id="email"
          name="email"
          label="Email Address"
          type="email"
          fullWidth
          variant="standard"
          value={email}
          onChange={(e) => setEmail(e.target.value)}
        />
        <TextField
          required
          margin="dense"
          id="password"
          name="password"
          label="Password"
          type="password"
          fullWidth
          variant="standard"
          value={password}
          onChange={(e) => setPassword(e.target.value)}
        />
      </DialogContent>
      <DialogActions>
        <Button type="submit">Login</Button>
        <Button onClick={onClose}>Cancel</Button>
      </DialogActions>
    </Box>
  );
};

export default Login;