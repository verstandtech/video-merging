import {Snackbar} from "@mui/material";
import React from "react";

type InfoMessageProps = {
  message: string,
  isOpen: boolean,
  onClose: () => void,
};

function InfoMessage({message, isOpen, onClose}: InfoMessageProps) {
  const handleClose = (event?: React.SyntheticEvent | Event, reason?: string) => {
    if (reason === 'clickaway') {
      return;
    }

    onClose();
  };

  return (
    <Snackbar
      open={isOpen}
      autoHideDuration={5000}
      onClose={handleClose}
      message={message}
    />
  );
}

export default InfoMessage;