import React from 'react';
import {Box} from '@mui/material';
import {IResultFile} from "./types";
import Empty from "./Empty";
import ResultPreview from "./ResultPreview";

type ResultListProps = {
  resultFiles: IResultFile[];
  selectedResultFiles: IResultFile[];
  onResultFileSelect: (file: IResultFile) => void;
};

class VideoList extends React.Component<ResultListProps> {
  render() {
    let {
      resultFiles,
      selectedResultFiles,
      onResultFileSelect,
    } = this.props;

    if (!Array.isArray(resultFiles)) return <Empty response={resultFiles}/>

    return (
      <Box sx={{
        display: 'flex',
        flexDirection: 'row',
        flexWrap: 'wrap',
        gap: '16px',
        overflowY: 'auto',
        width: '100%',
        height: '100vh'
      }}>
        {resultFiles.map((file) =>
          <Box sx={{minWidth: '300px'}}>
            <ResultPreview
              resultFile={file}
              key={file.filename}
              isResultSelected={selectedResultFiles.includes(file)}
              onResultSelect={onResultFileSelect}
            />
          </Box>
        )}
      </Box>
    );
  }
}

export default VideoList;