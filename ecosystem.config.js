module.exports = {
    apps: [
        {
            name: 'frontend',
            script: './start-frontend.js',
        },
        {
            name: 'backend',
            script: './start-backend.js',
        },
    ],
};