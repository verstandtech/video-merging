import React from 'react';
import ReportIcon from '@mui/icons-material/Report';
import { Box, Paper, Stack, Typography } from '@mui/material';
import { IError } from './types';

interface EmptyProps {
  response: IError;
}

const Empty: React.FC<EmptyProps> = ({ response }) => (
  <Box sx={{
    display: 'flex',
    flexDirection: 'row',
    flexWrap: 'wrap',
    gap: '16px',
    overflowY: 'auto',
    width: '100%',
    justifyContent: 'center',

  }}>
    <Box
      sx={{
        display: 'flex',
        justifyContent: 'center',
        alignItems: 'flex-start',
        height: '100vh',
        mt: 4,
      }}
    >
      <Paper
        elevation={3}
        sx={{
          display: 'flex',
          flexDirection: 'row',
          alignItems: 'center',
          justifyContent: 'center',
          padding: '20px',
          borderRadius: '10px',
          gap: '10px',
        }}
      >
        <ReportIcon sx={{ fontSize: 48, color: 'error.main' }} />
        <Stack>
          <Typography variant="h5" component="div" color="error.main" gutterBottom={true}>
            {response.data.message ?? 'Something went wrong'}
          </Typography>
        </Stack>
      </Paper>
    </Box>
  </Box>
);

export default Empty;