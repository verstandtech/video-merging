-- CreateTable
CREATE TABLE "Result" (
    "id" SERIAL NOT NULL,
    "title" VARCHAR(255) NOT NULL,
    "filename" VARCHAR(255) NOT NULL,
    "thumbnail" TEXT NOT NULL,
    "mimetype" TEXT NOT NULL,
    "add_time" TIMESTAMP(3) NOT NULL DEFAULT CURRENT_TIMESTAMP,
    "update_time" TIMESTAMP(3),

    CONSTRAINT "Result_pkey" PRIMARY KEY ("id")
);

-- CreateIndex
CREATE UNIQUE INDEX "Result_filename_key" ON "Result"("filename");
