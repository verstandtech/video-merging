import React, {FC, useState} from 'react';
import {Box, Button, DialogActions, DialogContent, DialogTitle, TextField} from "@mui/material";

interface SigninProps {
  onRegister: (data: { email: string; password: string }) => void;
  onClose: () => void;
}

const Signin: FC<SigninProps> = ({onRegister, onClose}) => {
  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");

  const handleRegister = (event: React.FormEvent) => {
    event.preventDefault();
    onRegister({email, password});
  };

  return (
    <Box component="form" onSubmit={handleRegister} autoComplete="off">
      <DialogTitle>
        Registration form
      </DialogTitle>
      <DialogContent>
        <TextField
          autoFocus
          required
          margin="dense"
          id="email"
          name="email"
          label="Email Address"
          type="email"
          fullWidth
          variant="standard"
          value={email}
          onChange={(e) => setEmail(e.target.value)}
        />
        <TextField
          autoFocus
          required
          margin="dense"
          id="password"
          name="password"
          label="Password"
          type="password"
          fullWidth
          variant="standard"
          value={password}
          onChange={(e) => setPassword(e.target.value)}
        />
      </DialogContent>
      <DialogActions>
        <Button type="submit">Register</Button>
        <Button onClick={onClose}>Cancel</Button>
      </DialogActions>
    </Box>
  )
};

export default Signin;