import React, {useState} from 'react';
import {Box, Checkbox, Typography} from '@mui/material';
import {IResultFile, IServerFile} from "./types";
import ModalWindow from "./Modal";

type VideoPreviewProps = {
  file: IServerFile | IResultFile;
  isSelected: boolean;
  onSelect: (file: IServerFile) => void;
};

const VideoPreview: React.FC<VideoPreviewProps> = ({file, isSelected, onSelect}) => {
  const [open, setOpen] = useState(false);

  const handleOpen = () => {
    setOpen(true);
  };

  const handleClose = () => {
    setOpen(false);
  };

  const handleVideoClick = () => {
    onSelect(file);
  }

  return (
    <Box sx={{
      display: 'flex',
      flexDirection: 'column',
      alignItems: 'center',
      margin: '5px',
      border: isSelected ? '2px solid #3f51b5' : '1px solid #e0e0e0',
      borderRadius: '10px',
      padding: '10px',
      cursor: 'pointer'
    }}>
      <Checkbox
        checked={isSelected}
        onChange={() => handleVideoClick()}
      />
      <Typography variant="h6" component="div" gutterBottom>
        {file.filename.slice(0, 20)}
      </Typography>
      <img
        style={{maxWidth: '100%', height: 'auto', borderRadius: '10px'}}
        src={`${process.env.REACT_APP_API_HOST}/files/file/${file.thumbnail}?thumbnail=true`}
        alt={file.title}
        onClick={handleOpen}
      />
      <ModalWindow
        open={open}
        handleClose={handleClose}
        content={<video
          style={{maxWidth: '100%', height: 'auto'}}
          controls
          src={`${process.env.REACT_APP_API_HOST}/files/file/${file.filename}?thumbnail=false`}
        />}/>
    </Box>
  );
};

export default VideoPreview;