import {
  ConflictException,
  Injectable,
  InternalServerErrorException,
  NotFoundException,
} from '@nestjs/common';
import { PrismaService } from '../prisma/prisma.service';
import { existsSync, mkdirSync, readdirSync, unlinkSync } from 'fs';
import { Files, Prisma, Result } from '@prisma/client';
import { writeFile } from 'fs/promises';
import * as ffmpeg from 'fluent-ffmpeg';
import * as path from 'path';
import { join } from 'path';
import { filenameFormat, thumbnailNameFormat } from './helpers/strings';

const uploadDir = './uploads';
const thumbnailDir = `${uploadDir}/thumbnails/`;
const resultDir = `${uploadDir}/results/`;
const resultThumbnailDir = `${resultDir}/thumbnails/`;

@Injectable()
export class FilesService {
  constructor(private prisma: PrismaService) {}

  async uploadFiles(
    files: Express.Multer.File[],
  ): Promise<Prisma.BatchPayload> {
    if (!existsSync(uploadDir)) {
      mkdirSync(uploadDir);
    }
    if (!existsSync(resultDir)) {
      mkdirSync(resultDir);
    }
    if (!existsSync(thumbnailDir)) {
      mkdirSync(thumbnailDir);
    }
    const filesData = files.map((file) => ({
      title: file.originalname,
      filename: filenameFormat(file.originalname),
      thumbnail: filenameFormat(thumbnailNameFormat(file.originalname)),
      mimetype: file.mimetype,
      size: file.size,
    }));
    const filenameList = filesData.map((item) => item.filename);

    const existingFiles = await this.prisma.files.findMany({
      where: {
        filename: {
          in: filenameList,
        },
      },
    });

    if (existingFiles.length) {
      const existingFilename = existingFiles.map((item) => item.filename);
      throw new ConflictException(
        `The following files already exist: ${existingFilename.toString()}`,
      );
    }

    await Promise.all(
      files.map(async (file) => {
        const videoFilename = `./uploads/${filenameFormat(file.originalname)}`;
        const thumbnailFilename = thumbnailNameFormat(
          path.basename(videoFilename),
        );
        const filePath = `${uploadDir}/${filenameFormat(file.originalname)}`;

        await writeFile(filePath, file.buffer);

        await new Promise((resolve, reject) => {
          ffmpeg(videoFilename)
            .on('end', resolve)
            .on('error', (err) =>
              reject(
                new Error(
                  `Error occurred with thumbnail generating: ${err.message}`,
                ),
              ),
            )
            .screenshots({
              count: 1,
              filename: thumbnailFilename,
              folder: thumbnailDir,
              size: '320x240',
            });
        });
      }),
    );

    return this.prisma.files.createMany({ data: filesData });
  }

  async getFiles(filter: string): Promise<Files[]> {
    if (!filter) filter = '';
    return this.prisma.files.findMany({
      where: {
        OR: [
          {
            title: { contains: filenameFormat(filter) },
          },
          {
            filename: { contains: filenameFormat(filter) },
          },
        ],
      },
    });
  }

  async getFile(filter: string): Promise<Files> {
    if (!filter) filter = '';
    return this.prisma.files.findFirst({
      where: {
        OR: [
          {
            title: { contains: filenameFormat(filter) },
          },
          {
            filename: { contains: filenameFormat(filter) },
          },
          {
            thumbnail: { contains: filenameFormat(filter) },
          },
        ],
      },
    });
  }

  async createFile(filenames: string[]): Promise<Result> {
    const formatFilenames = filenames.map((item) => filenameFormat(item));
    const fileNames: Partial<Files>[] = await this.prisma.files.findMany({
      where: {
        filename: {
          in: formatFilenames,
        },
      },
      select: { filename: true },
    });
    const filenameMap = new Map(fileNames.map((item) => [item.filename, item]));
    const sortedFileNames = formatFilenames.map((filename) =>
      filenameMap.get(filename),
    );
    const fileList = sortedFileNames.map((item) => item.filename);

    try {
      const data = await this.mergeVideos(fileList);
      return await this.prisma.result.create({
        data: {
          filename: data.filename,
          title: data.title,
          thumbnail: data.thumbnail,
          mimetype: data.mimetype,
        },
      });
    } catch (error) {
      throw new InternalServerErrorException(error);
    }
  }

  async deleteVideo(fileList: string[]): Promise<Prisma.BatchPayload> {
    const formatFileList = fileList.map((item) => filenameFormat(item));

    try {
      for (const file of formatFileList) {
        const filePath = join(uploadDir, file);
        const data = await this.getFile(file);
        if (!data) throw new NotFoundException('File not found');
        const thumbPath = join(thumbnailDir, data.thumbnail);

        if (existsSync(filePath)) unlinkSync(filePath);
        if (existsSync(thumbPath)) unlinkSync(thumbPath);
      }
    } catch (error) {
      throw new InternalServerErrorException(
        `An error occurred while deleting the videos: ${error.reponse.message}`,
      );
    }
    const result = await this.prisma.files.deleteMany({
      where: {
        filename: {
          in: formatFileList,
        },
      },
    });

    if (!result.count)
      throw new InternalServerErrorException(
        `An error occurred while deleting the videos`,
      );
    return result;
  }

  async mergeVideos(fileList: string[]): Promise<any> {
    const tempDir = './tempDir';
    if (!existsSync(tempDir)) {
      mkdirSync(tempDir, { recursive: true });
    }
    if (!existsSync(resultDir)) {
      mkdirSync(resultDir);
    }
    const resultFileName = `${Date.now()}.mp4`;
    const mergedVideoPath = `${resultDir}${resultFileName}`;
    const resultThumbnailFilename = filenameFormat(
      thumbnailNameFormat(resultFileName),
    );

    let command = ffmpeg();

    for (const fileName of fileList) {
      const filePath = `${uploadDir}/${fileName}`;
      const tempFilePath = `${tempDir}/${fileName}_temp.mp4`;

      if (!existsSync(filePath))
        throw new NotFoundException(`File not found: ${fileName}`);

      await new Promise((resolve, reject) => {
        ffmpeg(filePath)
          .format('mp4')
          .videoCodec('libx264')
          .size('1280x720')
          .outputOptions('-movflags faststart')
          .output(tempFilePath)
          .on('end', resolve)
          .on('error', reject)
          .run();
      });

      command = command.addInput(tempFilePath);
    }

    return new Promise((resolve, reject) => {
      command
        .mergeToFile(mergedVideoPath, tempDir)
        .screenshots({
          count: 1,
          filename: resultThumbnailFilename,
          folder: resultThumbnailDir,
          size: '320x240',
        })
        .on('end', async () => {
          readdirSync(tempDir).forEach((file) => {
            unlinkSync(`${tempDir}/${file}`);
          });
          const data = {
            filename: resultFileName,
            title: resultFileName,
            thumbnail: resultThumbnailFilename,
            mimetype: 'video/mp4',
          };
          resolve(data);
        })
        .on('error', reject);
    });
  }

  async getResultFiles(filter: string): Promise<Result[]> {
    if (!filter) filter = '';

    return this.prisma.result.findMany({
      where: {
        OR: [
          {
            title: { contains: filenameFormat(filter) },
          },
          {
            filename: { contains: filenameFormat(filter) },
          },
        ],
      },
    });
  }

  async getResult(filter: string): Promise<Result> {
    if (!filter) filter = '';
    return this.prisma.result.findFirst({
      where: {
        OR: [
          {
            title: { contains: filenameFormat(filter) },
          },
          {
            filename: { contains: filenameFormat(filter) },
          },
          {
            thumbnail: { contains: filenameFormat(filter) },
          },
        ],
      },
    });
  }

  async deleteResult(fileList: string[]): Promise<Prisma.BatchPayload> {
    const formatFileList = fileList.map((item) => filenameFormat(item));

    try {
      for (const file of formatFileList) {
        const filePath = join(resultDir, file);
        const data = await this.getResult(file);
        if (!data) throw new NotFoundException('Result file not found');
        const thumbPath = join(resultThumbnailDir, data.thumbnail);

        if (existsSync(filePath)) unlinkSync(filePath);
        if (existsSync(thumbPath)) unlinkSync(thumbPath);
      }
    } catch (error) {
      throw new InternalServerErrorException(
        `An error occurred while deleting the videos: ${error.message}`,
      );
    }
    const result = await this.prisma.result.deleteMany({
      where: {
        filename: {
          in: formatFileList,
        },
      },
    });

    if (!result.count)
      throw new InternalServerErrorException(
        'An error occurred while deleting the results files',
      );
    return result;
  }
}
