import React, { useState } from 'react';
import axios from 'axios';
import { ResultControlsProps } from './types';
import { Box, Button, ButtonGroup } from '@mui/material';
import InfoMessage from './Snackbar';

const ResultFileControls: React.FC<ResultControlsProps> = ({ selectedResults, onFilesChange, refreshResults }) => {
  const [snackbarOpen, setSnackbarOpen] = useState(false);
  const [snackbarMessage, setSnackbarMessage] = useState('');

  const handleDownload = () => {
    if (selectedResults.length === 0) {
      setSnackbarMessage('No files selected');
      setSnackbarOpen(true);
    } else {
      window.open(`${process.env.REACT_APP_API_HOST}/files/results/${encodeURIComponent(selectedResults[0].filename)}`);
    }
  };

  const handleDelete = () => {
    if (selectedResults.length === 0) {
      setSnackbarMessage('No files selected');
      setSnackbarOpen(true);
    } else {
      axios.delete(`${process.env.REACT_APP_API_HOST}/files/results/delete`, { data: selectedResults.map((file) => file.filename) })
        .then((response) => {
          if (response.data.count > 0) {
            setSnackbarMessage('File(s) deleted successfully');
          } else {
            setSnackbarMessage('No files deleted');
          }
          setSnackbarOpen(true);
          onFilesChange((prevFiles) =>
            prevFiles.filter(
              (file) => (!(selectedResults.some((selected) => selected.filename === file.filename))),
            ),
          );
          setSnackbarMessage('Files successfully deleted');
          setSnackbarOpen(true);
          refreshResults();
        })
        .catch(error => {
          setSnackbarMessage(error.response.data.message);
          setSnackbarOpen(true);
        });
    }
  };

  return (
    <Box
      sx={{
        display: 'flex',
        gap: '10px',
        flexDirection: 'column',
        alignItems: 'flex-start',
        padding: '16px',
        width: '100%',
        maxWidth: '90vw',
        flex: 1,
        borderRadius: 1,
        position: 'fixed',
        bottom: 0,
        zIndex: 1000,
        justifyContent: 'flex-end',
        direction: 'rtl',
      }}
    >
      <ButtonGroup variant="contained" sx={{ direction: 'ltr' }}>
        <Button color="primary" size="medium" onClick={handleDownload} disabled={selectedResults.length === 0}>
          Download
        </Button>
        <Button color="error" size="medium" onClick={handleDelete} disabled={selectedResults.length === 0}>
          Delete
        </Button>
      </ButtonGroup>
      <InfoMessage
        isOpen={snackbarOpen}
        onClose={() => setSnackbarOpen(false)}
        message={snackbarMessage}
      />
    </Box>


  );
};

export default ResultFileControls;