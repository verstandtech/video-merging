import React, { useCallback, useEffect, useState } from 'react';
import axios from 'axios';
import VideoControls from './component/VideoControls';
import VideoList from './component/VideoList';
import { IResultFile, IServerFile } from './component/types';
import MainMenu from './component/MainMenu';
import Modal from 'react-modal';
import { Container, Grid, Tab, Tabs } from '@mui/material';
import ResultList from './component/ResultList';
import ResultFileControls from './component/ResultFileControls';
import InfoMessage from './component/Snackbar';

function App() {
  const [selectedFiles, setSelectedFiles] = useState<IServerFile[]>([]);
  const [selectedResults, setSelectedResults] = useState<IResultFile[]>([]);
  const [files, setFiles] = useState<IServerFile[]>([]);
  const [resultFiles, setResultFiles] = useState<IResultFile[]>([]);
  // const [isLoggedIn, setIsLoggedIn] = useState<boolean>(true);
  const [value, setValue] = React.useState('files');
  const [snackbarMessage, setSnackbarMessage] = useState('');
  const [snackbarOpen, setSnackbarOpen] = useState(false);

  const fetchFiles = useCallback(() => {
    axios.get(`${process.env.REACT_APP_API_HOST}/files`)
      .then(response => setFiles(response.data))
      .catch(error => {
        if (error.response) {
          setFiles(error.response);
        }
      });
  }, []);

  const fetchResultFiles = useCallback(() => {
    axios.get(`${process.env.REACT_APP_API_HOST}/files/results`)
      .then(response => setResultFiles(response.data))
      .catch(error => {
        if (error.response) {
          setResultFiles(error.response);
        }
      });
  }, []);

  useEffect(() => {
    fetchFiles();
  }, [fetchFiles]);

  useEffect(() => {
    fetchResultFiles();
  }, [fetchResultFiles]);

  useEffect(() => {
    Modal.setAppElement('#app');
  }, []);

  const onFileSelect = (file: IServerFile) => {
    setSelectedFiles((prevSelectedFiles) =>
      prevSelectedFiles.some(f => f.filename === file.filename)
        ? prevSelectedFiles.filter((f) => f.filename !== file.filename)
        : [...prevSelectedFiles, file],
    );
  };

  const onResultSelect = (resultFile: IResultFile) => {
    setSelectedResults((prevSelectedResults) =>
      prevSelectedResults.some(f => f.filename === resultFile.filename)
        ? prevSelectedResults.filter((f) => f.filename !== resultFile.filename)
        : [...prevSelectedResults, resultFile],
    );
  };

  const onLogout = async () => {
    try {
      // TODO: Replace `/logout` with the correct logout route
      await axios.post(`${process.env.REACT_APP_API_HOST}/logout`);

      // Clear token
      localStorage.removeItem('token');
      // When ready to implement, this would set isLoggedIn to false:
      // setIsLoggedIn(false);
    } catch (error) {
      console.error('Failed to log out', error);
    }
  };

  return (
    <div id="app">
      <Container maxWidth={false}>
        <InfoMessage
          isOpen={snackbarOpen}
          onClose={() => setSnackbarOpen(false)}
          message={snackbarMessage}
        />
        <MainMenu isLoggedIn={false} onLogout={onLogout} />
        <Tabs value={value} onChange={(event, newValue) => setValue(newValue)} centered>
          <Tab label="Files" value="files" />
          <Tab label="Result" value="results" />
        </Tabs>
        {value === 'files' &&
          <Grid container direction={{ xs: 'column', sm: 'row' }}>
            <VideoList files={files} selectedFiles={selectedFiles} onFileSelect={onFileSelect} />
            <VideoControls selectedFiles={selectedFiles} onFilesChange={setSelectedFiles} refreshFiles={fetchFiles}
                           refreshResults={fetchResultFiles} />
          </Grid>}
        {value === 'results' &&
          <Grid container direction={{ xs: 'column', sm: 'row' }}>
            <ResultList resultFiles={resultFiles} selectedResultFiles={selectedResults}
                        onResultFileSelect={onResultSelect} />
            <ResultFileControls selectedResults={selectedResults} onFilesChange={setSelectedResults}
                                refreshResults={fetchResultFiles} />
          </Grid>}
      </Container>
    </div>
  );
}

export default App;