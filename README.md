# 🚀 Video Merging Application

This project holds a complete video merging application which allows users to upload their video files and then select
multiple files to merge into a single file.

## 💼 Getting Started

To get started, ensure that you have Node.js and npm (the Node Package Manager) installed on your machine.

Then, clone the project repository.

## 🎬 Install "ffmpeg"

**ffmpeg** is a powerful tool that can record, convert and stream audio and video. It is used in this
project for video file handling.

### Installation Steps

1. Open the [official ffmpeg page](https://ffmpeg.org/).
2. Follow the instructions for downloading and installing on your operating system.
3. After installation, you can verify it by running the following command in your terminal:

```bash 
ffmpeg -version
```

### Using FFmpeg in Our Project

In our project, we use ffmpeg for video processing. FFmpeg is a collection of free and open-source libraries that allow
you to record, convert, and stream digital audio and video in numerous formats.

### Required Codecs

FFmpeg employs various codecs to work with media files. During your installation of ffmpeg, you may need to include
additional codecs depending on your requirements. For example, in our project, we require the libx264 codec.
To install ffmpeg with additional codecs, you can follow the instructions mentioned under the Installing FFmpeg in the
Project section of this document.

## 📁 Project Structure

The project is structured into two distinct parts:

1. `backend/`: Holds the server-side code of the application, written in TypeScript
2. `client/`: Holds the client-side code of the application, written in TypeScript and React

### Setting up the Backend

- Navigate into the `backend/` directory and install the required dependencies:

```bash 
npm install
```

- Create a `.env` file in the root of the backend directory, based on the provided `.env.example` file.

### Setting up the Client

- Navigate into the `client/` directory and install the required dependencies:

```bash 
npm install
```

### Running the Application

- Make sure you have installed the required dependencies for both the client and server as mentioned above.
- A configuration file for PM2 has been provided to start both the frontend and backend simultaneously. To start the
  applications, simply run:

Run both the server application and the client application with following command:

```bash 
pm2 start ecosystem.config.js
```

This command will start both applications according to the settings defined in `ecosystem.config.js`. Each application
can be managed individually with PM2 by using their respective names: 'frontend' and 'backend'.

Note: Make sure to have PM2 installed globally. If it's not already installed, you can add it via npm:

```bash 
npm install pm2 -g
```

## 🎁 Features

The Video Merging Application provides these cool features:

- Upload video files
- Select multiple video files to merge
- Merge selected video files into a single file

## 📜 License

This project is licensed under the terms of the MIT license.