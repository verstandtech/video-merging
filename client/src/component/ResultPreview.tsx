import React, {useState} from 'react';
import {Box, Checkbox, Typography} from '@mui/material';
import {IResultFile} from "./types";
import ModalWindow from "./Modal";

type ResultPreviewProps = {
  resultFile: IResultFile;
  isResultSelected: boolean;
  onResultSelect: (file: IResultFile) => void;
};

const ResultPreview: React.FC<ResultPreviewProps> = ({resultFile, isResultSelected, onResultSelect}) => {
  const [open, setOpen] = useState(false);

  const handleOpen = () => {
    setOpen(true);
  };

  const handleClose = () => {
    setOpen(false);
  };

  const handleResultFileClick = () => {
    onResultSelect(resultFile);
  }

  return (
    <Box sx={{
      display: 'flex',
      flexDirection: 'column',
      alignItems: 'center',
      margin: '5px',
      border: isResultSelected ? '2px solid #3f51b5' : '1px solid #e0e0e0',
      borderRadius: '10px',
      padding: '10px',
      cursor: 'pointer',
    }}>
      <Checkbox
        checked={isResultSelected}
        onChange={() => handleResultFileClick()}
      />
      <Typography variant="h6" component="div" gutterBottom>
        {resultFile.filename.slice(0, 20)}
      </Typography>
      <img
        style={{maxWidth: '100%', height: 'auto', borderRadius: '10px'}}
        src={`${process.env.REACT_APP_API_HOST}/files/results/${resultFile.thumbnail}?thumbnail=true`}
        alt={resultFile.title}
        onClick={handleOpen}
      />
      <ModalWindow open={open} handleClose={handleClose} content={
        <video
          style={{maxWidth: '100%', height: 'auto'}}
          controls
          src={`${process.env.REACT_APP_API_HOST}/files/results/${resultFile.filename}?thumbnail=false`}
        />}/>
    </Box>
  );
};


export default ResultPreview;