import {
  Body,
  Controller,
  Get,
  HttpCode,
  HttpStatus,
  Param,
  Post,
  Put,
  Query,
} from '@nestjs/common';
import { UsersService } from './users.service';
import { EUser_role, Prisma, Users } from '@prisma/client';
import { ApiBody, ApiOperation, ApiResponse, ApiTags } from '@nestjs/swagger';

@Controller('users')
@ApiTags('Users')
export class UsersController {
  constructor(private readonly usersService: UsersService) {}

  @Get()
  @HttpCode(HttpStatus.FOUND)
  async getUsers(@Query('filter') filter?: string): Promise<Users[]> {
    return this.usersService.getUsers(filter);
  }

  @Get()
  @HttpCode(HttpStatus.FOUND)
  async getUser(dto: Users): Promise<Users> {
    return this.usersService.getUser(dto);
  }

  @Post('register')
  @HttpCode(HttpStatus.CREATED)
  @ApiOperation({ summary: 'Registration' })
  @ApiBody({
    type: 'object',
    description: 'Registration form',
    required: true,
    schema: {
      type: 'object',
      properties: {
        email: { type: 'string', example: 'user@example.com' },
        password: { type: 'string', example: 'mySecretPassword' },
        role: { type: 'string', example: 'user' },
      },
    },
  })
  @ApiResponse({ status: HttpStatus.OK, description: 'Success' })
  async createUser(
    @Body('email') email: string,
    @Body('password') password: string,
    @Body('role') role: EUser_role,
  ): Promise<Users> {
    const dto = {
      email: email,
      password: password,
      role: role,
    };
    return this.usersService.createUser(dto);
  }

  @Post('login')
  @HttpCode(HttpStatus.OK)
  @ApiOperation({ summary: 'Authorization' })
  @ApiBody({
    type: 'object',
    description: 'Authorization form',
    required: true,
    schema: {
      type: 'object',
      properties: {
        email: { type: 'string', example: 'user@example.com' },
        password: { type: 'string', example: 'mySecretPassword' },
      },
    },
  })
  @ApiResponse({ status: HttpStatus.OK, description: 'Success' })
  async login(
    @Body('email') email: string,
    @Body('password') password: string,
  ): Promise<Users> {
    return this.usersService.login(email, password);
  }

  @Put(':email')
  @HttpCode(HttpStatus.OK)
  async updateUser(
    @Param('email') email: string,
    @Body() dto: Prisma.UsersUpdateInput,
  ): Promise<Users> {
    return this.usersService.updateUser(email, dto);
  }
}
