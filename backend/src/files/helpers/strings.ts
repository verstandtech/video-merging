export const filenameFormat = (filename: string) => {
  return filename.replace(/\s/g, '_');
};

export const thumbnailNameFormat = (thumbnailName: string) => {
  return thumbnailName.replace(/\.[^/.]+$/, '.jpg');
};
