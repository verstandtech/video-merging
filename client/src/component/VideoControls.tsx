import React, { useState } from 'react';
import axios from 'axios';
import { VideoControlsProps } from './types';
import { Box, Button, ButtonGroup, Card, IconButton, Tooltip, Typography } from '@mui/material';
import DeleteIcon from '@mui/icons-material/Delete';
import ClearAllIcon from '@mui/icons-material/ClearAll';
import InfoMessage from './Snackbar';
import MovieIcon from '@mui/icons-material/Movie';

const VideoControls: React.FC<VideoControlsProps> = ({
                                                       selectedFiles,
                                                       onFilesChange,
                                                       refreshFiles,
                                                       refreshResults,
                                                     }) => {
  const [selectedFilesForUpload, setSelectedFilesForUpload] = useState<File[]>([]);
  const [snackbarMessage, setSnackbarMessage] = useState('');
  const [snackbarOpen, setSnackbarOpen] = useState(false);

  const handleFileSelect = (event: React.ChangeEvent<HTMLInputElement>) => {
    if (event.target.files) {
      setSelectedFilesForUpload(Array.from(event.target.files));
    }
  };

  const handleUpload = () => {
    const data = new FormData();
    selectedFilesForUpload.forEach((file) => {
      data.append('files', file);
    });

    axios.post(`${process.env.REACT_APP_API_HOST}/files/upload`, data, {
      headers: { 'Content-Type': 'multipart/form-data' },
    })
      .then(() => {
        setSelectedFilesForUpload([]);
        refreshFiles();
      })
      .catch((error) => {
        setSnackbarMessage(error.response.data.message);
        setSnackbarOpen(true);
      });
  };

  const handleCreate = () => {
    if (selectedFiles.length < 2) {
      setSnackbarMessage('Need to select more files');
      setSnackbarOpen(true);
    } else {
      setSnackbarMessage('Video files merging start');
      setSnackbarOpen(true);
      axios.post(`${process.env.REACT_APP_API_HOST}/files/create`, { titles: selectedFiles.map((file) => file.title) })
        .then((response) => {
          setSnackbarMessage(`Files "${response.data.filename}" successfully created`);
          setSnackbarOpen(true);
          refreshResults();
        })
        .catch((error) => {
          setSnackbarMessage(error.response.data.message);
          setSnackbarOpen(true);
        });
    }
  };

  const handleDelete = () => {
    if (selectedFiles.length === 0) {
      setSnackbarMessage('No files selected');
      setSnackbarOpen(true);
    } else {
      axios.delete(`${process.env.REACT_APP_API_HOST}/files/delete`, { data: selectedFiles.map((file) => file.filename) })
        .then((response) => {
          if (response.data.count > 0) {
            setSnackbarMessage('File(s) deleted successfully');
          } else {
            setSnackbarMessage('No files deleted');
          }
          setSnackbarOpen(true);
          onFilesChange((prevFiles) =>
            prevFiles.filter(
              (file) => !selectedFiles.find((selected) => selected.filename === file.filename),
            ),
          );
          refreshFiles();
        })
        .catch(error => {
          setSnackbarMessage(error.response.data.message);
          setSnackbarOpen(true);
        });
    }
  };

  const handleClearAll = () => {
    setSelectedFilesForUpload([]);
  };

  const handleRemoveFile = (index: number) => {
    setSelectedFilesForUpload(selectedFilesForUpload.filter((_file, i) => i !== index));
  };

  return (
    <Box
      sx={{
        display: 'flex',
        gap: '10px',
        flexDirection: 'column',
        alignItems: 'flex-start',
        padding: '16px',
        width: '100%',
        maxWidth: '90vw',
        flex: 1,
        borderRadius: 1,
        position: 'fixed',
        bottom: 0,
        zIndex: 1000,
        justifyContent: 'flex-end',
        direction: 'rtl',
      }}
    >
      {selectedFilesForUpload.length > 0 && (
        <Card
          sx={{
            display: 'flex',
            flexDirection: 'column',
            alignItems: 'center',
            justifyContent: 'center',
            gap: '10px',
            zIndex: 1000,
            borderRadius: 1,
            boxShadow: 2,
            padding: '16px',
            direction: 'ltr',
          }}>
          <Box sx={{
            display: 'flex',
            alignItems: 'center',
            width: '100%',
          }}>
            <Typography variant="body1">Selected Files:</Typography>
            <Tooltip title="Remove all files">
              <IconButton size="small" onClick={handleClearAll}>
                <ClearAllIcon />
              </IconButton>
            </Tooltip>
          </Box>
          <Box sx={{
            display: 'block',
            alignItems: 'center',
            gap: '10px',
          }}>
            {selectedFilesForUpload.map((file, index) => (
              <Box key={index} sx={{
                display: 'flex',
                alignItems: 'center',
              }}>
                <MovieIcon fontSize="small" />
                <Typography variant="body1">
                  {file.name.slice(0, 25)}
                </Typography>
                <Tooltip title="Remove file">
                  <IconButton size="small" onClick={() => handleRemoveFile(index)}>
                    <DeleteIcon />
                  </IconButton>
                </Tooltip>
              </Box>
            ))}
          </Box>
        </Card>
      )}

      {selectedFiles.length >= 1 && (
        <ButtonGroup variant="contained" sx={{ direction: 'ltr' }}>
          <Button color="primary" size="medium" onClick={handleCreate} disabled={selectedFiles.length === 0}>
            Create
          </Button>
          <Button color="error" size="medium" onClick={handleDelete} disabled={selectedFiles.length === 0}>
            Delete
          </Button>
        </ButtonGroup>
      )}
      <InfoMessage
        isOpen={snackbarOpen}
        onClose={() => setSnackbarOpen(false)}
        message={snackbarMessage}
      />
      {selectedFiles.length === 0 && (
        <ButtonGroup variant="contained" color="primary">
          <input
            type="file"
            multiple
            onChange={handleFileSelect}
            accept="video/*"
            style={{ display: 'none' }}
            id="contained-button-file"
          />
          <label htmlFor="contained-button-file">
            <Button component="span" size="medium"
                    style={{ display: selectedFilesForUpload.length === 0 ? 'inline-flex' : 'none' }}>Select
              Files</Button>
          </label>
          <Button size="medium" onClick={handleUpload}
                  style={{ display: selectedFilesForUpload.length !== 0 ? 'inline-flex' : 'none' }}>
            Upload
          </Button>
        </ButtonGroup>
      )}
    </Box>
  );
};

export default VideoControls;